%global _empty_manifest_terminate_build 0
Name:		python-gpxpy
Version:	1.6.2
Release:	1
Summary:	GPX file parser and GPS track manipulation library
License:	Apache 2.0
URL:		https://github.com/tkrajina/gpxpy
Source0:	https://files.pythonhosted.org/packages/20/ad/6f1a34e702c72cb495bb258396f237ded76c00f9fe67054a44d778d24ed9/gpxpy-1.6.2.tar.gz
BuildArch:	noarch


%description
gpx-py is a python GPX parser. GPX (GPS eXchange Format) is an XML based file format for GPS tracks.

%package -n python3-gpxpy
Summary:	GPX file parser and GPS track manipulation library
Provides:	python-gpxpy = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-gpxpy
gpx-py is a python GPX parser. GPX (GPS eXchange Format) is an XML based file format for GPS tracks.

%package help
Summary:	Development documents and examples for gpxpy
Provides:	python3-gpxpy-doc
%description help
gpx-py is a python GPX parser. GPX (GPS eXchange Format) is an XML based file format for GPS tracks.

%prep
%autosetup -n gpxpy-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-gpxpy -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Dec 07 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 1.6.2-1
- Update package to version 1.6.2

* Mon Sep 26 2022 liqiuyu <liqiuyu@kylinos.cn> - 1.5.0-1
- Upgrade package to version 1.5.0

* Fri May 20 2022 liukuo <liukuo@kylinos.cn> - 1.4.2-2
- License compliance rectification

* Fri Sep 03 2021 Python_Bot <Python_Bot@openeuler.org> - 1.4.2-1
- Package Init
